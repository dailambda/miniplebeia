open Miniplebeia.Plebeia

let test_tree_keys () =
  let open Tree in
  let check t ks =
    let ks' = gkeys t in
    if List.sort compare ks = List.sort compare ks' then ()
    else begin
      Format.eprintf "%a@.@.keys: [%a]@." Tree.pp t (Format.list "@ " pp_gkey) ks';
      assert false
    end
  in
  check bud_none [ [] ];
  check (leaf "hello") [ [] ];
  check (bud (leaf "hello")) [ [GBud ] ];
  check (bud (extender [Left] (leaf "hello"))) [ [ GBud; GLeft ] ];
  check (bud (extender [Left] bud_none)) [ [ GBud; GLeft ] ];
  check (bud (extender [Left] (bud (leaf "hello")))) [ [ GBud; GLeft; GBud ] ];
  check (bud (extender [Left] (bud (extender [Right] (leaf "hello"))))) [ [ GBud; GLeft; GBud; GRight ] ];
  check (bud (internal (leaf "hello") (leaf "bye"))) [ [ GBud; GLeft ]; [ GBud; GRight ] ];
  ()

let non_existent_key keys =
  let open Gen in
  let rec f = function
    | [] ->
        int 8 >>= (function
            | 0 ->
                f [] >|= fun k -> GBud::k
            | 1 ->
                f [] >|= fun k -> GLeft::k
            | 2 ->
                f [] >|= fun k -> GRight::k
            | _ -> return [])
    | ks ->
        int 3 >>= function
            | 0 ->
                let ks = List.filter_map (function
                    | GBud::ks -> Some ks
                    | _ -> None) ks
                in
                f ks >|= fun k -> GBud :: k
            | 1 ->
                let ks = List.filter_map (function
                    | GLeft::ks -> Some ks
                    | _ -> None) ks
                in
                f ks >|= fun k -> GLeft::k
            | _ ->
                let ks = List.filter_map (function
                    | GRight::ks -> Some ks
                    | _ -> None) ks
                in
                f ks >|= fun k -> GRight::k
  in
  f keys

let test_tree_get ?seed () =
  (* It only tests successful cases *)
  let rng = Gen.init ?seed () in
  for _ = 0 to 1_000 do
    let open Gen in
    run rng
      (Tree.gen >>= fun t ->
       let ks = Tree.gkeys t in
       List.iter (fun k ->
           match Tree.gget t k with
           | None ->
               Format.eprintf "key %a@.@.tree %a@."
                 pp_gkey k Tree.pp t;
               assert false
           | Some (Leaf _) | Some (Bud None) -> ()
           | _ ->
               Format.eprintf "key %a@.@.tree %a@."
                 pp_gkey k Tree.pp t;
               assert false) ks;
       list (return 100) (non_existent_key ks) >>= fun nks ->
       List.iter (fun nk ->
           match Tree.gget t nk with
           | None -> ()
           | Some _ ->
               Format.eprintf "nkey %a@.@.tree %a@."
                 pp_gkey nk Tree.pp t;
               assert false
         ) nks;
       return ())
  done

let test_merkle_proof ?seed () =
  let rng = Gen.init ?seed () in
  for _ = 0 to 1_000_000 do
    let open Gen in
    run rng
      (Tree.gen >>= fun t ->
       let root = Tree.hash t in
       Merkle_proof.gen t >|= fun p ->
       assert (Merkle_proof.hash p = root);
       ())
  done

let test_merkle_proof_make ?seed () =
  let rng = Gen.init ?seed () in
  for _ = 0 to 1_000 do
    let open Gen in
    run rng
      (Tree.gen >>= fun t ->
       let root = Tree.hash t in
       let ks = Tree.gkeys t in
       let n_ks = List.length ks in
       (let open Gen in
        list (int n_ks) (one_of (Array.of_list ks))) >>= fun cks ->
       (let open Gen in
        list (int n_ks) (non_existent_key ks)) >>= fun nks ->
       let p, not_found = Merkle_proof.gmake t (cks @ nks) in
       let print () =
         Format.eprintf "tree %a@.@.proof %a@.@.keys [ %a ]@.@.nks [ %a ]@.@.not_found [ %a ]@."
           Tree.pp t Merkle_proof.pp p
           (Format.list ";@ " pp_gkey) cks
           (Format.list ";@ " pp_gkey) nks
           (Format.list ";@ " pp_gkey) not_found;
       in
       if Merkle_proof.hash p <> root then begin
         print ();
         assert false
       end;
       List.iter (fun k ->
           match Merkle_proof.gget p k with
           | `Tree (Tree.Leaf _) -> ()
           | `Tree (Tree.Bud None) -> ()
           | `Found _ -> assert false
           | _ ->
               print ();
               Format.eprintf "key %a@." pp_gkey k;
               assert false) cks;
       List.iter (fun nk ->
           match Merkle_proof.gget p nk with
           | `Not_found -> ()
           | r ->
               print ();
               Format.eprintf "nkey %a@." pp_gkey nk;
               begin match r with
                 | `Tree t -> Format.eprintf "tree %a@." Tree.pp t
                 | `Found t -> Format.eprintf "proof %a@." Merkle_proof.pp t
                 | `Unknown -> Format.eprintf "unknown@."
                 | `Not_found -> assert false
               end;
               assert false) nks;
       return ())
  done

let test_diff ?seed () =
  let rng = Gen.init ?seed () in
  for _ = 0 to 1_000_000 do
    let open Gen in
    run rng
      (Tree.gen >>= fun t1 ->
       Tree.gen >|= fun t2 ->
       let diff = Diff.diff t1 t2 in
       (match Diff.apply (Some t1) diff = Some t2 with
       | true -> ()
       | exception e ->
           Format.eprintf "%a@.@.%a@.@.%a@." Tree.pp t1 Tree.pp t2 Diff.pp diff;
           raise e
       | false ->
           Format.eprintf "%a@.@.%a@.@.%a@." Tree.pp t1 Tree.pp t2 Diff.pp diff;
           assert false);
       let diff' = Diff_with_proof.diff t1 t2 in
       assert (Diff_with_proof.strip diff' = diff);

       begin match Diff_with_proof.proof1 diff' with
         | None -> ()
         | Some p1 ->
             if Merkle_proof.hash p1 = Tree.hash t1 then ()
             else begin
               Format.eprintf "%a@.@.%a@.@.%a@.@.%a@.@.%a@."
                 Tree.pp t1 Tree.pp t2 Diff.pp diff
                 Diff_with_proof.pp diff'
                 Merkle_proof.pp p1
               ;
               assert false
             end
       end;

       begin match Diff_with_proof.proof2 diff' with
         | None -> ()
         | Some p2 ->
             if Merkle_proof.hash p2 = Tree.hash t2 then ()
             else begin
               Format.eprintf "%a@.@.%a@.@.%a@.@.%a@.@.%a@."
                 Tree.pp t1 Tree.pp t2 Diff.pp diff
                 Diff_with_proof.pp diff'
                 Merkle_proof.pp p2
               ;
               assert false
             end
       end;

       let h1, h2 = Diff_with_proof.hashes diff' in
       let p () =
         Format.eprintf "%a@.@.%a@.@.%a@.@.%a@."
           Tree.pp t1 Tree.pp t2 Diff.pp diff
           Diff_with_proof.pp diff'
       in
       match h1, h2 with
       | None, _ | _, None ->
           p ();
           assert false
       | Some h1, Some h2 ->
           if h1 <> Tree.hash t1 then begin
             p ();
             assert false
           end;
           if h2 <> Tree.hash t2 then begin
             p ();
             assert false
           end
      )
  done

(* The test may FAIL by a hash collision of Hashtbl.hash *)
let () =
  test_tree_keys ();
  test_tree_get ();
  test_merkle_proof_make ();

  test_merkle_proof ();
  test_diff ();
