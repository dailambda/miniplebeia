type value = string [@@deriving show]

type side = Left | Right [@@deriving show]

type segment = side list [@@deriving show]

type key = segment list [@@deriving show]

type hash_prefix = int [@@deriving show]

type hash = hash_prefix * value [@@deriving show]

type gside = GLeft | GRight | GBud
[@@deriving show]

type gkey = gside list
[@@deriving show]

val string_of_gkey : gkey -> string

val gkey_of_segment : segment -> gkey

(** Assuming [key] starts from a [Bud] *)
val gkey_of_key : key -> gkey

module Key : sig
  val empty : key
  val snoc : key -> (gside * key) option
  val cons : gside -> key -> key
end

module Format : sig
  include module type of struct include Format end
  type 'a t = Format.formatter -> 'a -> unit
  val option : 'a t -> 'a option t
  val list : (unit, formatter, unit) format -> 'a t -> 'a list t
end

module Gen : sig
  type 'a t = Random.State.t -> 'a
  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
  val ( >|= ) : 'a t -> ('a -> 'b) -> 'b t
  val int : int -> int t
  val list : int t -> 'a t -> 'a list t
  val value : value t
  val bool : bool t
  val segment : segment t
  val one_of : 'a array -> 'a t
  val init : ?seed:hash_prefix -> unit -> Random.State.t
  val run : 'a -> ('a -> 'b) -> 'b
end

module Tree : sig
  type t =
    | Bud of (hash_prefix * t) option
    | Internal of hash_prefix * t * t
    | Extender of segment * t
    | Leaf of hash_prefix * value
  [@@deriving show]

  (** Returns the hash of the tree precomputed *)
  val hash : t -> hash

  (** Builders *)
  val bud : t -> t

  val bud_none : t

  val internal : t -> t -> t

  (** With concatenating nested Extenders *)
  val extender : segment -> t -> t

  (** Build an Extender if one of the args is [None] *)
  val internal_opt : t option -> t option -> t

  val leaf : value -> t

  val get : t -> key -> t option

  val keys : t -> key list

  val gget : t -> gkey -> t option

  val gkeys : t -> gkey list

  val gen : t Gen.t
end

module Merkle_proof : sig
  type t =
    | Bud of t
    | Internal of t * t
    | Extender of segment * t
    | Hash of hash
    | Tree of Tree.t
  [@@deriving show]

  (** Verification, trusting the hash of [Tree.t] *)
  val hash : t -> hash

  (** Query in the proof
      [`Unknown] : the key points at outside of the proof
      [`Found] : the key points at a subtree of the proof
      [`Tree] : the key points at a Plebeia tree
      [`Not_found] : the key points at nothing
  *)
  val get : t -> key -> [`Unknown | `Found of t | `Tree of Tree.t | `Not_found]

  val make : Tree.t -> key list -> t * key list
  (** Create the proof of the given keys for the tree.
      Non existent keys are returned with the proof.
  *)

  val gget : t -> gkey -> [`Unknown | `Found of t | `Tree of Tree.t | `Not_found]

  val gmake : Tree.t -> gkey list -> t * gkey list

  (** Generate a random Merkle_proof of the input *)
  val gen : Tree.t -> t Gen.t
end

module Diff : sig
  type t =
    | Bud of t
    | Internal of t * t
    | Extender of segment * t
    | Command of command

  and command = Remove | Add of Tree.t | Replace of Tree.t | Same
  [@@deriving show]

  val diff : Tree.t -> Tree.t -> t

  (** [None] for the null tree *)
  val apply : Tree.t option -> t -> Tree.t option
end

module Diff_with_proof : sig
  type t =
    | Bud of t
    | Internal of t * t
    | Extender of segment * t
    | Command of command

  and command =
    | Remove of hash (** hash of the original *)
    | Add of Tree.t
    | Replace of hash (** hash of the original *) * Tree.t
    | Same of hash (** hash of the both sub-trees *)
  [@@deriving show]

  val diff : Tree.t -> Tree.t -> t

  (** [None] for the null tree *)
  val apply : Tree.t option -> t -> Tree.t option

  val strip : t -> Diff.t

  val proof1 : t -> Merkle_proof.t option

  val proof2 : t -> Merkle_proof.t option

  val hashes : t -> hash option * hash option
end
