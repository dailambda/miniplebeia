(* Plebeia tree, its diff with Merkle proof *)

type value = string
[@@deriving show]

type side = Left | Right
[@@deriving show]

type segment = side list
[@@deriving show]

type key = segment list
[@@deriving show]

(* Classical key = segment list has several difficulties:

   - Impossible to pin-point the node under a bud
   - A cons corresponds with a Bud, but the root Bud is an exception.
   - Code becomes complex.

   Adding explicit information of Bud to keys makes the things much clearer.
*)
type gside = GLeft | GRight | GBud
[@@deriving show]

type gkey = gside list
[@@deriving show]

let gkey_of_segment = List.map (function Left -> GLeft | Right -> GRight)

let gkey_of_key ss =
  (* assuming it starts from a bud *)
  List.concat (List.map (fun s -> GBud :: gkey_of_segment s) ss)

let string_of_segment s =
  let buf = Bytes.create (List.length s) in
  List.iteri (fun i side ->
      Bytes.unsafe_set buf i (match side with Left -> 'L' | Right -> 'R'))
    s;
  Bytes.unsafe_to_string buf

let string_of_gkey s =
  let buf = Bytes.create (List.length s) in
  List.iteri (fun i side ->
      Bytes.unsafe_set buf i (match side with GLeft -> 'L' | GRight -> 'R' | GBud -> 'B'))
    s;
  Bytes.unsafe_to_string buf

module Key = struct
  let empty = [[]]

  let snoc = function
    | [] -> assert false
    | [[]] -> None
    | []::ss -> Some (GBud, ss)
    | (Left::s)::ss -> Some (GLeft, s::ss)
    | (Right::s)::ss -> Some (GRight, s::ss)

  let cons gside ss =
    match gside, ss with
    | _, [] -> assert false
    | GBud, ss -> []::ss
    | GLeft, s::ss -> (Left::s)::ss
    | GRight, s::ss -> (Right::s)::ss
end

type hash_prefix = int
[@@deriving show]

type hash = hash_prefix * string (* LRLR.. *)
[@@deriving show]

module Format = struct
  include Format

  type 'a t = Format.formatter -> 'a -> unit

  let option f ppf = function
    | None -> pp_print_string ppf "None"
    | Some x -> Format.fprintf ppf "Some (%a)" f x

  let rec list  (sep : (unit, formatter, unit) format) p ppf = function
    | [] -> ()
    | [x] -> p ppf x
    | x::xs ->
        fprintf ppf "@[%a@]%t%a"
          p x
          (fun ppf -> fprintf ppf sep)
          (list sep p) xs
end

module List = struct
  include List

  let partition_map f xs =
    let ls, rs =
      List.fold_left (fun (ls,rs) x ->
          match f x with
          | `Left x -> x::ls, rs
          | `Right x -> ls, x::rs ) ([], []) xs
    in
    List.rev ls, List.rev rs
end

module Gen = struct
  open Random.State

  type 'a t = Random.State.t -> 'a

  let return a = fun _ -> a

  let (>>=) : 'a t -> ('a -> 'b t) -> 'b t = fun at f ->
    fun rng -> let a = at rng in f a rng

  let (>|=) : 'a t -> ('a -> 'b) -> 'b t = fun at f ->
    fun rng -> let a = at rng in f a

  let int sz rng = int rng sz

  let list (len : int t) (v : 'a t) : 'a list t =
    len >>= fun l rng ->
    List.init l (fun _ -> v rng)

  let value =
    int 16 >>= fun len ->
    list (return len) (int 23 >|= fun i -> Char.(chr (code 'a' + i)))
    >|= fun cs ->
    let buf = Bytes.create len in
    List.iteri (fun i c -> Bytes.unsafe_set buf i c) cs;
    Bytes.unsafe_to_string buf

  let bool = int 2 >|= function 0 -> false | _ -> true

  let segment =
    list (int 16 >|= (+) 1) (int 2 >|= function 0 -> Left | _ -> Right)

  let init ?seed () =
    let seed = match seed with
      | None ->
          Random.self_init (); Random.int (1 lsl 30 - 1)
      | Some seed -> seed
    in
    Random.State.make [| seed |]

  let one_of xs = int (Array.length xs) >|= fun i -> Array.unsafe_get xs i

  let run rng at = at rng
end

let hashpf tag hs = Hashtbl.hash (tag, hs)

(* Plebeia tree, simplified *)
module Tree = struct
  (* Patricia tree + Bud + hashes *)
  type t =
    | Bud of (hash_prefix * t) option
    | Internal of hash_prefix * t * t
    | Extender of segment * t
    | Leaf of hash_prefix * value
  [@@deriving show]

  let rec hash = function
    | Bud (Some (h,_)) -> h, ""
    | Bud None -> 0, ""
    | Internal (h, _, _) -> h, ""
    | Extender (s, t) ->
        let h, s' = hash t in
        (* Extender must not have an Extender,
           but we can still compute a valid hash for nested Extenders *)
        h, string_of_segment s ^ s'
    | Leaf (h, _) -> h, ""

  let bud t =
    let h = hashpf 0 [hash t] in
    Bud (Some (h, t))

  let bud_none = Bud None

  let internal t1 t2 =
    let h = hashpf 1 [hash t1; hash t2] in
    Internal (h, t1, t2)

  let raw_extender s t =
    match t with
    | Extender _ -> assert false
    | _ -> assert (s <> []); Extender (s, t)

  let extender s t =
    match s with
    | [] -> t
    | _ ->
        match t with
        | Extender (s', t) -> raw_extender (s @ s') t
        | _ -> raw_extender s t

  let internal_opt t1o t2o =
    match t1o, t2o with
    | None, None -> assert false
    | Some t1, Some t2 -> internal t1 t2
    | Some t1, None -> extender [Left] t1
    | None, Some t2 -> extender [Right] t2

  let leaf v =
    let h = Hashtbl.hash (2, v) in
    Leaf (h, v)

  let rec get t key =
    match t, key with
    | Extender ([], t0), _ -> get t0 key
    | _, [] -> None
    | _, [[]] -> Some t
    | Bud (Some (_, t0)), []::key -> get t0 key
    | Bud (Some _), _ -> None
    | Bud None, _ -> None
    | Internal _, []::_ -> None
    | Internal (_, t1, _), (Left::k)::ks -> get t1 (k::ks)
    | Internal (_, _, t2), (Right::k)::ks -> get t2 (k::ks)
    | Extender (side::s,t0), (side'::k)::ks when side = side' ->
        get (Extender (s,t0)) (k::ks)
    | Extender _, _ -> None
    | Leaf _, _ -> None

  let rec gget t key =
    match t, key with
    | Extender ([], t0), _ -> gget t0 key
    | _, [] -> Some t
    | Bud (Some (_, t0)), GBud::key -> gget t0 key
    | Bud (Some _), _ -> None
    | Bud None, _ -> None
    | Internal (_, t1, _), GLeft::ks -> gget t1 ks
    | Internal (_, _, t2), GRight::ks -> gget t2 ks
    | Internal _, _ -> None
    | Extender (Left::s,t0), GLeft::ks
    | Extender (Right::s,t0), GRight::ks -> gget (Extender (s,t0)) ks
    | Extender _, _ -> None
    | Leaf _, _ -> None

  let rec keys : t -> key list = function
    | Leaf _ -> [ [[]] ]
    | Bud None -> [ [[]] ]
    | Bud (Some (_, t)) -> List.map (fun k -> []::k) @@ keys t
    | Extender (s, t0) ->
        let ks = keys t0 in
        List.map (function
            | [] -> [s]
            | k::ks -> (s @ k)::ks) ks
    | Internal (_, t1, t2) ->
        let keys1 = keys t1 in
        let keys2 = keys t2 in
        List.map (function
            | [] -> [[Left]]
            | k::ks -> (Left::k)::ks) keys1
        @ List.map (function
            | [] -> [[Right]]
            | k::ks -> (Right::k)::ks) keys2

  let rec gkeys : t -> gkey list = function
    | Leaf _ -> [ [] ]
    | Bud None -> [ [] ]
    | Bud (Some (_, t)) -> List.map (fun k -> GBud::k) @@ gkeys t
    | Extender (s, t0) ->
        let ks = gkeys t0 in
        List.map (fun gp -> (gkey_of_segment s) @ gp) ks
    | Internal (_, t1, t2) ->
        let keys1 = gkeys t1 in
        let keys2 = gkeys t2 in
        List.map (fun k -> GLeft :: k) keys1
        @ List.map (fun k -> GRight :: k) keys2

  let rec gen rng =
    let open Gen in
    (int 4 >>= function
      | 0 ->
          int 2 >>= (function
          | 0 -> return bud_none
          | _ ->
              (* XXX Bud must not have Bud *)
              gen >|= fun t -> bud t)
      | 1 ->
          gen >>= fun t1 ->
          gen >|= fun t2 -> internal t1 t2
      | 2 ->
          segment >>= fun s ->
          gen >|= fun t ->
          extender s t
      | _ ->
          value >|= leaf) rng

end

module Merkle_proof = struct
  (* Merkle proof of Plebeia tree *)
  type t =
    | Bud of t
    | Internal of t * t
    | Extender of segment * t
    | Hash of hash
    | Tree of Tree.t (* We can strip the hashes of [Tree.t] *)
  [@@deriving show]

  (* Verification, trusting the hash of [Tree.t] *)
  let rec hash = function
    | Bud t -> hashpf 0 [hash t], ""
    | Internal (t1, t2) -> hashpf 1 [hash t1; hash t2], ""
    | Extender (s, t) ->
        let h, s' = hash t in
        h, string_of_segment s ^ s'
    | Hash h -> h
    | Tree t -> Tree.hash t

  let extender s t = match t with
    | Extender (s', t) -> Extender (s @ s', t)
    | _ -> Extender (s, t)

  let internal_opt t1 t2 = match t1, t2 with
    | None, None -> assert false
    | Some t1, Some t2 -> Internal (t1, t2)
    | Some t1, None -> extender [Left] t1
    | None, Some t2 -> extender [Right] t2

  let rec get t key =
    match t, key with
    | Extender ([], t0), _ -> get t0 key
    | Hash _, _ -> `Unknown
    | Tree t, _ ->
        (match Tree.get t key with
         | Some t -> `Tree t
         | None -> `Not_found)
    | _, [] -> `Unknown
    | _, [[]] -> `Found t
    | Bud t0, []::key -> get t0 key
    | Bud _, _ -> `Not_found
    | Internal _, []::_ -> `Not_found
    | Internal (t1, _), (Left::k)::ks -> get t1 (k::ks)
    | Internal (_, t2), (Right::k)::ks -> get t2 (k::ks)
    | Extender (side::s,t0), (side'::k)::ks when side = side' ->
        get (Extender (s,t0)) (k::ks)
    | Extender _, _ -> `Not_found

  let rec gget t key =
    match t, key with
    | Extender ([], t0), _ -> gget t0 key
    | Hash _, _ -> `Unknown
    | Tree t, _ ->
        (match Tree.gget t key with
         | Some t -> `Tree t
         | None -> `Not_found)
    | _, [] -> `Found t
    | Bud t0, GBud::key -> gget t0 key
    | Bud _, _ -> `Not_found
    | Internal (t1, _), GLeft::ks -> gget t1 ks
    | Internal (_, t2), GRight::ks -> gget t2 ks
    | Internal _,  _ -> `Not_found
    | Extender (Left::s,t0), GLeft::ks
    | Extender (Right::s,t0), GRight::ks ->
        gget (Extender (s,t0)) ks
    | Extender _, _ -> `Not_found

  let make t keys =
    let module T = Tree in
    let rec f t = function
      | [] ->
          Hash (T.hash t), []
      | keys ->
          match t with
          | T.Bud (Some (_,t0)) ->
              let empties, keys =
                List.partition (function
                    | [[]], _ -> true
                    | _, _ -> false) keys
              in

              let rej, keys =
                List.partition_map (function
                    | [[]], _ -> assert false
                    | []::k, korg -> `Right (k, korg)
                    | _, korg -> `Left korg) keys
              in
              let p, rej' = f t0 keys in
              let rej = rej @ rej' in
              if empties = [] then Bud p, rej
              else Tree t, rej
          | Bud None ->
              let empties, rejs =
                List.partition_map (function
                    | ([[]],_) -> `Left ()
                    | (_,korg) -> `Right korg) keys
              in
              if empties = [] && rejs = [] then Hash (T.hash t), rejs
              else Tree t, rejs
          | Leaf _ ->
              let empties, rejs =
                List.partition_map (function
                    | ([[]],_) -> `Left ()
                    | (_,korg) -> `Right korg) keys
              in
              if empties = [] && rejs = [] then Hash (T.hash t), rejs
              else Tree t, rejs
          | Internal (_, t1, t2) ->
              let empties, keys =
                List.partition (function
                    | ([],_) -> true
                    | _ -> false) keys
              in
              let rejs, keys =
                List.partition_map (function
                    | [], _ -> assert false
                    | []::_, korg -> `Left korg
                    | k -> `Right k) keys
              in
              let ls, rs =
                List.partition_map (function
                    | [],_ -> assert false
                    | []::_,_ -> assert false
                    | (Left::k)::ks, korg -> `Left (k::ks, korg)
                    | (Right::k)::ks, korg -> `Right (k::ks, korg)) keys
              in
              let lp, lrejs = f t1 ls in
              let rp, rrejs = f t2 rs in
              if empties <> [] then Tree t, rejs @ lrejs @ rrejs
              else Internal (lp, rp), rejs @ lrejs @ rrejs
          | Extender ([], t0) -> f t0 keys
          | Extender (Left::s, t0) ->
              let empties, keys =
                List.partition (function
                    | ([],_) -> true
                    | _ -> false) keys
              in
              let keys, rejs =
                List.partition_map (function
                    | (Left::k)::ks, korg -> `Left (k::ks, korg)
                    | _, korg -> `Right korg) keys
              in
              let p, rejs' = f (Extender (s, t0)) keys in
              if empties <> [] then Tree t, rejs @ rejs'
              else extender [Left] p, rejs @ rejs'
          | Extender (Right::s, t0) ->
              let empties, keys =
                List.partition (function
                    | ([],_) -> true
                    | _ -> false) keys
              in
              let rejs, keys =
                List.partition_map (function
                    | (Right::k)::ks, korg -> `Right (k::ks, korg)
                    | _, korg -> `Left korg) keys
              in
              let p, rejs' = f (Extender (s, t0)) keys in
              if empties <> [] then Tree t, rejs @ rejs'
              else extender [Right] p, rejs @ rejs'
    in
    f t (List.map (fun k -> (k,k)) keys)

  let gmake t keys =
    let module T = Tree in
    let rec f t = function
      | [] ->
          Hash (T.hash t), []
      | keys ->
          match t with
          | T.Bud (Some (_,t0)) ->
              let empties, keys =
                List.partition (function
                    | [], _ -> true
                    | _, _ -> false) keys
              in

              let rej, keys =
                List.partition_map (function
                    | [], _ -> assert false
                    | GBud::k, korg -> `Right (k, korg)
                    | _, korg -> `Left korg) keys
              in
              let p, rej' = f t0 keys in
              let rej = rej @ rej' in
              if empties = [] then Bud p, rej
              else Tree t, rej
          | Bud None ->
              let empties, rejs =
                List.partition_map (function
                    | ([],_) -> `Left ()
                    | (_,korg) -> `Right korg) keys
              in
              if empties = [] && rejs = [] then Hash (T.hash t), rejs
              else Tree t, rejs
          | Leaf _ ->
              let empties, rejs =
                List.partition_map (function
                    | ([],_) -> `Left ()
                    | (_,korg) -> `Right korg) keys
              in
              if empties = [] && rejs = [] then Hash (T.hash t), rejs
              else Tree t, rejs
          | Internal (_, t1, t2) ->
              let empties, keys =
                List.partition (function
                    | [],_ -> true
                    | _ -> false) keys
              in
              let rejs, keys =
                List.partition_map (function
                    | [], _ -> assert false
                    | GBud::_, korg -> `Left korg
                    | k -> `Right k) keys
              in
              let ls, rs =
                List.partition_map (function
                    | [],_ -> assert false
                    | GBud::_,_ -> assert false
                    | GLeft::ks, korg -> `Left (ks, korg)
                    | GRight::ks, korg -> `Right (ks, korg)) keys
              in
              let lp, lrejs = f t1 ls in
              let rp, rrejs = f t2 rs in
              if empties <> [] then Tree t, rejs @ lrejs @ rrejs
              else Internal (lp, rp), rejs @ lrejs @ rrejs
          | Extender ([], t0) -> f t0 keys
          | Extender (Left::s, t0) ->
              let empties, keys =
                List.partition (function
                    | ([],_) -> true
                    | _ -> false) keys
              in
              let keys, rejs =
                List.partition_map (function
                    | GLeft::ks, korg -> `Left (ks, korg)
                    | _, korg -> `Right korg) keys
              in
              let p, rejs' = f (Extender (s, t0)) keys in
              if empties <> [] then Tree t, rejs @ rejs'
              else extender [Left] p, rejs @ rejs'
          | Extender (Right::s, t0) ->
              let empties, keys =
                List.partition (function
                    | ([],_) -> true
                    | _ -> false) keys
              in
              let rejs, keys =
                List.partition_map (function
                    | GRight::ks, korg -> `Right (ks, korg)
                    | _, korg -> `Left korg) keys
              in
              let p, rejs' = f (Extender (s, t0)) keys in
              if empties <> [] then Tree t, rejs @ rejs'
              else extender [Right] p, rejs @ rejs'
    in
    f t (List.map (fun k -> (k,k)) keys)

  (* Randomly cull Plebeia subtrees and obtain a proof *)
  let rec gen t =
    let open Gen in
    int 3 >>= function
    | 0 -> return @@ Hash (Tree.hash t)
    | 1 -> return @@ Tree t
    | _ ->
        match t with
        | Tree.Leaf _
        | Bud None -> return @@ Tree t
        | Bud (Some (_, t)) ->
            gen t >|= fun t -> Bud t
        | Internal (_, t1, t2) ->
            gen t1 >>= fun t1 ->
            gen t2 >|= fun t2 -> Internal (t1, t2)
        | Extender (s, t) ->
            gen t >|= fun t -> Extender (s, t)
end

(* Diff between 2 Plebeia trees *)
module Diff = struct
  (* Patricia tree with diff commands *)
  type t =
    | Bud of t
    | Internal of t * t
    | Extender of segment * t
    | Command of command

  and command =
    | Remove
    | Add of Tree.t
    | Replace of Tree.t
    | Same
  [@@deriving show]

  let extender s t =
    if s = [] then t
    else
      match t with
      | Extender (s', t) -> Extender (s @ s', t)
      | _ -> Extender (s, t)

  let rec diff t1 t2 =
    let h1 = Tree.hash t1 in
    let h2 = Tree.hash t2 in
    if h1 = h2 then Command Same
    else
      match t1, t2 with
      | Tree.Leaf (_, v1), Tree.Leaf (_, v2) when v1 = v2 -> Command Same
      | Internal (_, t11, t12), Internal (_, t21, t22) ->
          let d1 = diff t11 t21 in
          let d2 = diff t12 t22 in
          begin match d1, d2 with
          | Command Same, Command Same -> Command Same
          | _ -> Internal (d1, d2)
          end
      | Internal (_, t11, _), Extender (Left::s, t20) ->
          let t21 = Tree.extender s t20 in
          let d1 = diff t11 t21 in
          begin match d1 with
            | Command Same -> Internal (Command Same, Command Remove)
            | _ -> Internal (d1, Command Remove)
          end
      | Internal (_, _, t12), Extender (Right::s, t20) ->
          let t22 = Tree.extender s t20 in
          let d2 = diff t12 t22 in
          begin match d2 with
            | Command Same -> Internal (Command Remove, Command Same)
            | _ -> Internal (Command Remove, d2)
          end

      | Extender ([], t10), _ -> diff t10 t2
      | _, Extender ([], t20) -> diff t1 t20
      | Extender (Left::s1, t10), Extender (Left::s2, t20) ->
          begin match diff (Tree.extender s1 t10) (Tree.extender s2 t20) with
            | Command Same -> Command Same
            | d -> extender [Left] d
          end

      | Extender (Right::s1, t10), Extender (Right::s2, t20) ->
          begin match diff (Tree.extender s1 t10) (Tree.extender s2 t20) with
            | Command Same -> Command Same
            | d -> extender [Right] d
          end

      | Extender (Left::_, _t10), Extender (Right::s2, t20) ->
          Internal (Command Remove, Command (Add (Tree.extender s2 t20)))

      | Extender (Right::_, _t10), Extender (Left::s2, t20) ->
          Internal (Command (Add (Tree.extender s2 t20)), Command Remove)

      | Extender (Left::s1, t10), Internal (_, t21, t22) ->
          let d1 = diff (Tree.extender s1 t10) t21 in
          begin match d1 with
            | Command Same -> Internal (Command Same, Command (Add t22))
            | d1 -> Internal (d1, Command (Add t22))
          end

      | Extender (Right::s1, t10), Internal (_, t21, t22) ->
          let d2 = diff (Tree.extender s1 t10) t22 in
          begin match d2 with
            | Command Same -> Internal (Command (Add t21), Command Same)
            | d2 -> Internal (Command (Add t21), d2)
          end

      | Bud (Some (_, t10)), Bud (Some (_, t20)) ->
          begin match diff t10 t20 with
            | Command Same -> Command Same
            | d -> Bud d
          end
      | Bud None, Bud (Some (_, t20)) ->
          Bud (Command (Add t20))
      | Bud (Some _), Bud None ->
          Bud (Command Remove)
      | Bud None, Bud None -> Command Same

      | Internal _, Bud _
      | Internal _, Leaf _
      | Bud _, Internal _
      | Bud _, Extender _
      | Bud _, Leaf _
      | Leaf _, Leaf _
      | Leaf _, Bud _
      | Leaf _, Internal _
      | Leaf _, Extender _
      | Extender _, Leaf _
      | Extender _, Bud _
        ->
          Command (Replace t2)

  let rec apply t diff =
    let module T = Tree in
    match t, diff with
    | _, Command (Replace t) -> Some t
    | _, Command (Add t) -> Some t
    | _, Command Remove -> None
    | _, Command Same -> t (* ??? *)
    | _, Extender ([], d) -> apply t d
    | Some (T.Extender ([], t)), _ -> apply (Some t) diff
    | None, (Bud _ | Internal _ | Extender _) -> assert false
    | Some t, diff ->
        let t = match t, diff with
          | _, Command _ -> assert false
          | _, Extender ([], _) -> assert false
          | T.Leaf _, _ -> assert false
          | Bud (Some (_, t)), Bud d ->
              begin match apply (Some t) d with
                | None -> T.bud_none
                | Some t -> T.bud t
              end
          | Bud None, Bud d ->
              begin match apply None d with
                | None -> assert false
                | Some t -> T.bud t
              end
          | Bud _, _ ->
              Format.eprintf "%a@.%a@."
                T.pp t pp diff;
              assert false
          | Internal _, Extender _ -> assert false
          | Internal (_, t1, t2), Internal (d1, d2) ->
              T.internal_opt (apply (Some t1) d1) (apply (Some t2) d2)
          | Internal _, _ -> assert false
          | Extender (Left::s, t), Extender (Left::s', d) ->
              begin match apply (Some (T.extender s t)) (extender s' d) with
                | None -> assert false
                | Some t -> T.extender [Left] t
              end
          | Extender (Right::s, t), Extender (Right::s', d) ->
              begin match apply (Some (T.extender s t)) (extender s' d) with
                | None -> assert false
                | Some t -> T.extender [Right] t
              end
          | Extender (Left::s,t), Internal (d1, d2) ->
              T.internal_opt (apply (Some (T.extender s t)) d1) (apply None d2)
          | Extender (Right::s,t), Internal (d1, d2) ->
              T.internal_opt (apply None d1) (apply (Some (T.extender s t)) d2)
          | _ ->
              Format.eprintf "%a@.%a@."
                T.pp t pp diff;
              assert false
        in
        Some t
end

(* Diff between 2 Plebeia trees *)
module Diff_with_proof = struct
  (* Patricia tree with diff commands *)
  type t =
    | Bud of t
    | Internal of t * t
    | Extender of segment * t
    | Command of command

  (* Diff commands with the hash of the original sub-tree *)
  and command =
    | Remove of hash
    | Add of Tree.t
    | Replace of hash * Tree.t
    | Same of hash
  [@@deriving show]

  let extender s t =
    if s = [] then t
    else
      match t with
      | Extender (s', t) -> Extender (s @ s', t)
      | _ -> Extender (s, t)

  module T = Tree

  let rec diff t1 t2 =
    let h1 = T.hash t1 in
    let h2 = T.hash t2 in
    if h1 = h2 then Command (Same h1)
    else
      match t1, t2 with
      | T.Leaf (_, v1), T.Leaf (_, v2) when v1 = v2 -> Command (Same h1)
      | Internal (_, t11, t12), Internal (_, t21, t22) ->
          let d1 = diff t11 t21 in
          let d2 = diff t12 t22 in
          begin match d1, d2 with
          | Command (Same _), Command (Same _) -> Command (Same h1)
          | _ -> Internal (d1, d2)
          end
      | Internal (_, t11, t12), Extender (Left::s, t20) ->
          let t21 = T.extender s t20 in
          Internal (diff t11 t21, Command (Remove (T.hash t12)))
      | Internal (_, t11, t12), Extender (Right::s, t20) ->
          let t22 = T.extender s t20 in
          Internal (Command (Remove (T.hash t11)), diff t12 t22)
      | Extender ([], t10), _ -> diff t10 t2
      | _, Extender ([], t20) -> diff t1 t20
      | Extender (Left::s1, t10), Extender (Left::s2, t20) ->
          begin match diff (T.extender s1 t10) (T.extender s2 t20) with
            | Command (Same _) -> Command (Same h1)
            | d -> extender [Left] d
          end

      | Extender (Right::s1, t10), Extender (Right::s2, t20) ->
          begin match diff (T.extender s1 t10) (T.extender s2 t20) with
            | Command (Same _) -> Command (Same h1)
            | d -> extender [Right] d
          end

      | Extender (Left::s, t10), Extender (Right::s2, t20) ->
          Internal (Command (Remove (T.hash @@ T.extender s t10)), Command (Add (T.extender s2 t20)))

      | Extender (Right::s, t10), Extender (Left::s2, t20) ->
          Internal (Command (Add (T.extender s2 t20)), Command (Remove (T.hash @@ T.extender s t10)))

      | Extender (Left::s1, t10), Internal (_, t21, t22) ->
          Internal (diff (T.extender s1 t10) t21, Command (Add t22))

      | Extender (Right::s1, t10), Internal (_, t21, t22) ->
          Internal (Command (Add t21), diff (T.extender s1 t10) t22)

      | Bud (Some (_, t10)), Bud (Some (_, t20)) ->
          begin match diff t10 t20 with
            | Command (Same _) -> Command (Same h1)
            | d -> Bud d
          end
      | Bud None, Bud (Some (_, t20)) ->
          Bud (Command (Add t20))
      | Bud (Some (_,t)), Bud None ->
          Bud (Command (Remove (T.hash t)))
      | Bud None, Bud None -> Command (Same h1)

      | Internal _, Bud _
      | Internal _, Leaf _
      | Bud _, Internal _
      | Bud _, Extender _
      | Bud _, Leaf _
      | Leaf _, Leaf _
      | Leaf _, Bud _
      | Leaf _, Internal _
      | Leaf _, Extender _
      | Extender _, Leaf _
      | Extender _, Bud _
        ->
          Command (Replace (h1, t2))

  let rec apply t diff =
    let module T = Tree in
    match t, diff with
    | _, Command (Replace (_,t)) -> Some t
    | _, Command (Add t) -> Some t
    | _, Command (Remove _)-> None
    | _, Command (Same _) -> t (* ??? *)
    | _, Extender ([], d) -> apply t d
    | Some (T.Extender ([], t)), _ -> apply (Some t) diff
    | None, (Bud _ | Internal _ | Extender _) -> assert false
    | Some t, diff ->
        let t = match t, diff with
          | _, Command _ -> assert false
          | _, Extender ([], _) -> assert false
          | T.Leaf _, _ -> assert false
          | Bud (Some (_, t)), Bud d ->
              begin match apply (Some t) d with
                | None -> T.bud_none
                | Some t -> T.bud t
              end
          | Bud None, Bud d ->
              begin match apply None d with
                | None -> assert false
                | Some t -> T.bud t
              end
          | Bud _, _ ->
              Format.eprintf "%a@.%a@."
                T.pp t pp diff;
              assert false
          | Internal _, Extender _ -> assert false
          | Internal (_, t1, t2), Internal (d1, d2) ->
              T.internal_opt (apply (Some t1) d1) (apply (Some t2) d2)
          | Internal _, _ -> assert false
          | Extender (Left::s, t), Extender (Left::s', d) ->
              begin match apply (Some (T.extender s t)) (extender s' d) with
                | None -> assert false
                | Some t -> T.extender [Left] t
              end
          | Extender (Right::s, t), Extender (Right::s', d) ->
              begin match apply (Some (T.extender s t)) (extender s' d) with
                | None -> assert false
                | Some t -> T.extender [Right] t
              end
          | Extender (Left::s,t), Internal (d1, d2) ->
              T.internal_opt (apply (Some (T.extender s t)) d1) (apply None d2)
          | Extender (Right::s,t), Internal (d1, d2) ->
              T.internal_opt (apply None d1) (apply (Some (T.extender s t)) d2)
          | _ ->
              Format.eprintf "%a@.%a@."
                T.pp t pp diff;
              assert false
        in
        Some t

  module MP = Merkle_proof

  module D = Diff

  let rec strip = function
    | Bud d -> D.Bud (strip d)
    | Internal (d1, d2) -> D.Internal (strip d1, strip d2)
    | Extender (s, d) -> Extender (s, strip d)
    | Command (Remove _) -> D.Command D.Remove
    | Command (Add t) -> D.Command (D.Add t)
    | Command (Replace (_, t)) -> D.Command (D.Replace t)
    | Command (Same _) -> D.Command D.Same

  let rec proof1 = function
    | Bud d ->
        begin match proof1 d with
          | None -> Some (MP.Hash (T.hash T.bud_none))
          | Some p -> Some (MP.Bud p)
        end
    | Internal (d1, d2) ->
        begin match proof1 d1, proof1 d2 with
          | None, None -> None
          | p1, p2 -> Some (MP.internal_opt p1 p2)
        end
    | Extender (s, d) ->
        begin match proof1 d with
          | None -> None
          | Some p -> Some (MP.extender s p)
        end
    | Command (Remove h) -> Some (MP.Hash h)
    | Command (Add _) -> None
    | Command (Replace (h, _)) -> Some (MP.Hash h)
    | Command (Same h) -> Some (MP.Hash h)

  let rec proof2 = function
    | Bud d ->
        begin match proof2 d with
          | None -> Some (MP.Hash (T.hash T.bud_none))
          | Some p -> Some (MP.Bud p)
        end
    | Internal (d1, d2) ->
        begin match proof2 d1, proof2 d2 with
          | None, None -> None
          | p1, p2 -> Some (MP.internal_opt p1 p2)
        end
    | Extender (s, d) ->
        begin match proof2 d with
          | None -> None
          | Some p -> Some (MP.extender s p)
        end
    | Command (Remove _) -> None
    | Command (Add t) -> Some (MP.Tree t)
    | Command (Replace (_, t)) -> Some (MP.Tree t)
    | Command (Same h) -> Some (MP.Hash h)

  let rec hashes = function
    | Bud d ->
        let f = function
          | None -> Some (T.hash T.bud_none)
          | Some h -> Some (hashpf 0 [h], "")
        in
        let h1,h2 = hashes d in
        f h1, f h2
    | Internal (d1, d2) ->
        let f h1 h2 = match h1, h2 with
          | None, None -> assert false
          | Some h1, Some h2 -> Some (hashpf 1 [h1; h2], "")
          | Some (hp1, s1), None -> Some (hp1, "L" ^ s1)
          | None, Some (hp1, s1) -> Some (hp1, "R" ^ s1)
        in
        let h11, h12 = hashes d1 in
        let h21, h22 = hashes d2 in
        f h11 h21, f h12 h22
    | Extender (s, d) ->
        let f = function
          | None -> None
          | Some (hp, s') -> Some (hp, string_of_segment s ^ s')
        in
        let h1, h2 = hashes d in
        f h1, f h2
    | Command (Remove h) -> Some h, None
    | Command (Add t) -> None, Some (T.hash t)
    | Command (Replace (h, t)) -> Some h, Some (T.hash t)
    | Command (Same h) -> Some h, Some h
end
