# Plebeia tree and Merkle proofs

This is a mini reference implementation of Plebeia tree and its Merkle proof.

## `Tree`

Plebeia tree: Patricia tree + Bud (directory) + Merkle hashes.

* No persistency to disk
* No optimization of segments (tree labels)

## `Merkle_proof`

Merkle proof of Plebeia tree

## `Diff`

Difference between 2 Plebeia trees, arranged as a patricia tree.

## `Diff_with_proof`

Difference of Plebeia trees with Merkle proof.

A proof of each tree is obtained from this diff with a proof cheaply.

## Tests

Random test of Merkle proof, diff, and diff with Merkle proof.

